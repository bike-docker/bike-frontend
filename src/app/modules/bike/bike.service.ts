import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IBike } from './bike.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BikeService {

  constructor(private http: HttpClient ) { }

  /**
   * Method Query all data
   */

  public query(): Observable<IBike[]> {
    return this.http.get<IBike[]>(`${environment.END_POINT}/api/bikes`)
    .pipe(map( res => {
      return res;
    }));
  }// End method query

  // Other methos
  
  public saveBike(bike: IBike): Observable<IBike> {
     return this.http.post<IBike>(`${environment.END_POINT}/api/bikes`,bike)
     .pipe(map(res => {
       return res;
     }));   
  }// end method saveBike

  public getById(id: string): Observable<IBike> {
    return this.http.get<IBike>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    }));   
  }

  /**
   * this method is for update bike entity
   * @param bike 
   */

  public update(bike: IBike): Observable<IBike> {
    return this.http.put<IBike>(`${environment.END_POINT}/api/bikes`,bike)
    .pipe(map(res => {
      return res;
    }));  
  }


}
