import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ClientsService } from '../../clients/clients.service';
import { IClient } from '../../clients/client';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

  client: IClient;

  searchForm = this.fb.group({
    document: '',
    firstName: ''
  });

  constructor(
    private fb: FormBuilder,
    private clientService: ClientsService
  ) { }

  ngOnInit() {
  }

  searchClient():  void {
    console.warn('DOCUMENTO... ',this.searchForm.value.document);

    this.clientService.searchClientByDocument(this.searchForm.value.document)
    .subscribe(res => {
      console.warn('RES ',res);
      this.client = res[0];
    });
  }
}
