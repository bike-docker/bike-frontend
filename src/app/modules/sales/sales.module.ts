import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesRoutingModule } from './sales-routing.module';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [CatalogoComponent],
  imports: [
    CommonModule,
    SalesRoutingModule,
    ReactiveFormsModule
  ]
})
export class SalesModule { }
