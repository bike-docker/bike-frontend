import { IBike } from '../bike/bike.model';
import { IClient } from '../clients/client';

export interface ISale {
    id: number;
    date: Date;
    clientId: number;
    clientName: string;
    bikeId: number;
    bikeSerial: string;
    bike: IBike;
    client: IClient; 
}

